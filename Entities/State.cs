﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CountySSGoogle.Entities
{
    [Table("d_state")]
    public class State
    {
        [Key]
        [Column("state_id")]
        public int id { get; set; }
        public string state_abbrev { get; set; }
        public string state_name { get; set; }
    }
}
