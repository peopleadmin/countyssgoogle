﻿using CountySSGoogle.Persistence;
using CountySSGoogle.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Threading.Tasks;

namespace CountySSGoogle
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            bool result = false;
            string currentDateTime = DateTime.Now.ToString("yyyyMMdd_HHmm");
            Log.Logger = new LoggerConfiguration()
                    .WriteTo.Console()
                    .WriteTo.File($"Logs_{currentDateTime}.log")
                    .CreateLogger();

            //         .WriteTo.File($"C:\\JagsSparkHire\\Logs_{currentDateTime}.log")

            IConfiguration configuration = new ConfigurationBuilder()
                           .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                           .AddEnvironmentVariables()
                           .AddCommandLine(args)
                           .Build();

            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection, configuration);

            var serviceProvider = serviceCollection.BuildServiceProvider();

            var logger = serviceProvider.GetService<ILogger<Program>>();
            try
            {                
                logger.LogInformation($"BEGINNING COUNTY CHK");
                result = await serviceProvider.GetService<CountyService>().DoCountyStuff();
                logger.LogInformation("THE END!");
            }
            catch (Exception ex)
            {
                logger.LogError("ERROR OCCURRED!");
            }
            if (result)
            {
                Console.WriteLine("COMPLETED SUCCESSFULLY!");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("FAILED!");
                Console.ReadKey();
            }
        }

        private static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            //we will configure logging here
            services.AddLogging(configure => configure.AddSerilog());

            services.AddSingleton<CountyService>();

            services.AddDbContextPool<AppDbContext>(options => options.UseMySql(configuration.GetConnectionString("Ssv2Context")));
            services.AddScoped<IDataRepository, DataRepository>();
        }
    }


}
