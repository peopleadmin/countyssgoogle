﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CountySSGoogle.Entities
{
    [Table("d_county")]
    public class County
    {
        [Key]
        public int county_id { get; set; }
        [Column("state_id")]
        public int stateid { get; set; }
        public int country_id { get; set; }
        public string county_name { get; set; }
        public State state { get; set; }
    }
}
