﻿using CountySSGoogle.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountySSGoogle.Persistence
{
    public class DataRepository : IDataRepository
    {
        private readonly AppDbContext _appDbContext;

        public DataRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public async Task<IEnumerable<County>> GetCounties()
        {
            return await _appDbContext.counties
                .Include(c=>c.state).ToListAsync();
        }
    }
}
