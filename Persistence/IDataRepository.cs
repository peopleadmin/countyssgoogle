﻿using CountySSGoogle.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountySSGoogle.Persistence
{
    public interface IDataRepository
    {
        Task<IEnumerable<County>> GetCounties();
    }
}