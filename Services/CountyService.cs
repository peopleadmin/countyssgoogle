﻿using CountySSGoogle.Entities;
using CountySSGoogle.Persistence;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CountySSGoogle.Services
{
    public class CountyService
    {
        private readonly ILogger<CountyService> _logger;
        private readonly IDataRepository _dataRepository;

        public CountyService(ILogger<CountyService> logger, IDataRepository dataRepository)
        {
            _logger = logger;
            _dataRepository = dataRepository;
        }

        public async Task<bool> DoCountyStuff()
        {
            IEnumerable<County> counties = await _dataRepository.GetCounties();
            string address, CountyShortName = String.Empty, CountyLongName = String.Empty, CountyLocalityShortName = String.Empty, CountyLocalityLongName = String.Empty;
            string StateShortName = String.Empty, StateLongName = String.Empty, CountryShortName = String.Empty, CountryLongName = String.Empty;
            var httpClient = new HttpClient();

            //ParallelLoopResult parallelLoopResult = Parallel.ForEach(counties, county => ChkWithGoogle(county));
            bool firstApi = true;
            string ApiUrl;
            foreach (var county in counties)
            {
                address = $"{county.county_name+county.state.state_abbrev}";
                address = HttpUtility.UrlEncode(address);
                CountyShortName = String.Empty;
                CountyLongName = String.Empty;
                CountyLocalityShortName = String.Empty;
                CountyLocalityLongName = String.Empty;

                if (firstApi)
                {
                    ApiUrl = $"https://maps.googleapis.com/maps/api/geocode/json?&address={address}&key=AIzaSyDSPtmP8JQ4UGflkC-GN7F_MWb3181q31U";
                    try
                    {
                        using (var response = await httpClient.GetAsync(ApiUrl))
                        {
                            var apiResponse = await response.Content.ReadAsStringAsync();
                            JObject o = JObject.Parse(apiResponse);
                            JArray addrComponents = (JArray)o["results"][0]["address_components"];

                            foreach (var addr in addrComponents)
                            {
                                if (addr["types"][0].ToString() == "administrative_area_level_2")
                                {
                                    CountyLongName = addr["long_name"].ToString();
                                    CountyShortName = addr["short_name"].ToString();
                                }
                                if (addr["types"][0].ToString() == "locality")
                                {
                                    CountyLocalityLongName = addr["long_name"].ToString();
                                    CountyLocalityShortName = addr["short_name"].ToString();
                                }
                                if (addr["types"][0].ToString() == "administrative_area_level_1")
                                {
                                    StateLongName = addr["long_name"].ToString();
                                    StateShortName = addr["short_name"].ToString();
                                }
                                if (addr["types"][0].ToString() == "country")
                                {
                                    CountryLongName = addr["long_name"].ToString();
                                    CountryShortName = addr["short_name"].ToString();
                                } 
                            }
                            _logger.LogInformation($"County#{county.county_id},NameInDB#{county.county_name},LocalityLong#{CountyLocalityLongName},LocalityShort#{CountyLocalityShortName},CountyLong#{CountyLongName},CountyShort#{CountyShortName},StateID#{county.stateid},StateAbbrev#{county.state.state_abbrev},StateShort#{StateShortName},StateName#{county.state.state_name},StateLong#{StateLongName},CountryLong#{CountryLongName},CountryShort#{CountryShortName},ApiUrl#{ApiUrl}");

                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInformation($"Exception API1: {ex.Message}");
                        firstApi = false;
                    }
                }                
                else
                {
                    ApiUrl = $"https://maps.googleapis.com/maps/api/geocode/json?&address={address}&key=AIzaSyCmtrO79E1a0VLCoaMP2Q-qKRToxfYsLGg";
                    try
                    {
                        using (var response = await httpClient.GetAsync(ApiUrl))
                        {
                            var apiResponse = await response.Content.ReadAsStringAsync();
                            JObject o = JObject.Parse(apiResponse);
                            JArray addrComponents = (JArray)o["results"][0]["address_components"];

                            foreach (var addr in addrComponents)
                            {
                                if (addr["types"][0].ToString() == "administrative_area_level_2")
                                {
                                    CountyLongName = addr["long_name"].ToString();
                                    CountyShortName = addr["short_name"].ToString();
                                }
                                if (addr["types"][0].ToString() == "locality")
                                {
                                    CountyLocalityLongName = addr["long_name"].ToString();
                                    CountyLocalityShortName = addr["short_name"].ToString();
                                }
                                if (addr["types"][0].ToString() == "administrative_area_level_1")
                                {
                                    StateLongName = addr["long_name"].ToString();
                                    StateShortName = addr["short_name"].ToString();
                                }
                                if (addr["types"][0].ToString() == "country")
                                {
                                    CountryLongName = addr["long_name"].ToString();
                                    CountryShortName = addr["short_name"].ToString();
                                }                               
                            }
                            _logger.LogInformation($"County#{county.county_id},NameInDB#{county.county_name},LocalityLong#{CountyLocalityLongName},LocalityShort#{CountyLocalityShortName},CountyLong#{CountyLongName},CountyShort#{CountyShortName},StateID#{county.stateid},StateAbbrev#{county.state.state_abbrev},StateShort#{StateShortName},StateName#{county.state.state_name},StateLong#{StateLongName},CountryLong#{CountryLongName},CountryShort#{CountryShortName},ApiUrl#{ApiUrl}");

                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInformation($"Exception API2: {ex.Message}");
                        firstApi = true;
                    }
                }
            }

            return true;
        }

        private void ChkWithGoogle(County county)
        {
            throw new NotImplementedException();
        }
    }
}
